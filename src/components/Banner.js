import {Row, Col} from 'react-bootstrap';

export default function Banner({bannerData}){
	return(
		<Row>
			<Col>
				<h1 className="text-center mt-5">{bannerData.title}</h1>
				<h4 className="text-center my-4">{bannerData.content}</h4>
			</Col>
		</Row>

		);
}