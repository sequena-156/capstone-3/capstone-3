import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}) {
	return (			
					<Card className="cardTo">
						<Card.Body>
							<Card.Title>
								{productProp.name}
							</Card.Title>
							 <Card.Img variant="top" className="prodImg" src={`${productProp._id}.JPG`} />
							<Card.Text>
								Price: {productProp.price}
							</Card.Text>
							<Link to={`view/${productProp._id}`} className="btn btn-secondary prodBtn">
								View Product
							</Link>
						</Card.Body>
					</Card>
					
		);
};