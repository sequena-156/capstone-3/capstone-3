import {Navbar, Nav, NavDropdown, Container} from 'react-bootstrap';
import { useContext } from 'react';

import UserContext from '../UserContext';
export default function AppNavBar() {

    const { user } = useContext(UserContext);

    return(
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        
  <Container>
  <Nav>
  <a href="/">
          <img src="/LSlogo.png"width="60"
         height="40" alt="/"/>
        </a></Nav>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="me-auto">
    {
      user.id !== null ? true
      :
      <Nav.Link href="/register">Sign Up</Nav.Link>
    }
      <NavDropdown title={user.isAdmin ? "Admin" : "Menu"} id="collapsible-nav-dropdown">
    {
      user.isAdmin ?  <NavDropdown.Item href="/products/all">All Products</NavDropdown.Item>
      : true
    }
      {
        user.isAdmin ?
        <>
        <NavDropdown.Item href="/products/add">Create Product</NavDropdown.Item>
        </>
        :
        <>
        <NavDropdown.Item href="#socials">Follow Us</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="/#aboutLakbay">About Lakbay Sportswear</NavDropdown.Item>
        </>
      }
      </NavDropdown>
      
    </Nav>
  </Navbar.Collapse>
    <Nav>
      
         <Nav.Link href="/products">{user.isAdmin ? "Available Products":"Shop Now"}</Nav.Link>
      
      {
        user.id !== null ?
        <>
          <Nav.Link href="/logout">Logout</Nav.Link>
        </>
        :
        <Nav.Link eventKey={2} href="/login">
          Login
        </Nav.Link>
      }
    </Nav>
  </Container>
</Navbar>

        )
      };