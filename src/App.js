import { useState, useEffect } from 'react';
import './App.css';

import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import ErrorPage from './pages/Error';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Logout from './pages/Logout';
import AllProducts from './pages/AllProducts';
import AddProduct from './pages/AddProduct';
import UpdateProduct from './pages/UpdateProduct';

import { UserProvider } from './UserContext';

import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () =>{
    localStorage.clear();
    setUser({
      id: null,
      isAdmin: null
    });
  }

  useEffect(() => {

    let token = localStorage.getItem('accessToken');

    fetch('https://frozen-bayou-69638.herokuapp.com/users/details', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(res => res.json()).then(convertedData => {
      if (typeof convertedData._id !== "undefined") {
        setUser({
          id: convertedData._id,
          isAdmin: convertedData.isAdmin
        });
      } else {
        setUser({
          id: null,
          isAdmin: null
        });
      }
    });
  },[user]);

  return (
    <UserProvider value={{user, setUser, unsetUser}} >
      <Router>
         <AppNavBar />
         <Routes>
          <Route path='/' element={<Home/>} />
          <Route path='/login' element={<Login/>} />
          <Route path='/register' element={<Register/>} />
          <Route path='/products' element={<Products />} />
          <Route path='/products/all' element={<AllProducts />} />
          <Route path='/products/view/:id' element={<ProductView />} />
          <Route path='/products/add' element={<AddProduct />} />
          <Route path='/products/all/update/:id' element={<UpdateProduct />} />
          <Route path='/logout' element={<Logout />} />
          <Route path='*' element={<ErrorPage/>} />
         </Routes> 
      </Router>
    </UserProvider>
  );
}

export default App;
