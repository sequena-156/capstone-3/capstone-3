//identify the components needed to create the register page.
import { useState, useEffect, useContext } from 'react'
import {Container,Row,Col, Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register() {

	const { user } = useContext(UserContext);

	const [firstName, setFirstName] = useState('');
	const [middleName, setMiddleName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2]  = useState('');

	const [isActive, setIsActive] = useState(false);
	const [isMatched, setIsMatched] = useState(false);
	const [isMobileValid, setIsMobileValid] = useState(false);
	const [isAllowed, setIsAllowed] = useState(false);

	
	useEffect(() => {

		if (
				mobileNo.length === 11
		)
			{
				setIsMobileValid(true)
				if (
					password1 !== ''&& password2 !==''&& password1 === password2
				) {
					setIsMatched(true);
					if (firstName !== '' && middleName !== '' && lastName !== '' && email!=='') {
						setIsAllowed(true);
						setIsActive(true);

					} else {
						setIsAllowed(false);
						setIsActive(false);
					}
				} else {
					setIsMatched(false);
					setIsAllowed(false);
					setIsActive(false);
				}

			}
		else if(password1 !=='' && password1 === password2){
			setIsMatched(true);	
		} else {
			setIsActive(false);
			setIsMatched(false);
			setIsMobileValid(false);
			setIsAllowed(false);
		};
	},[firstName, lastName, middleName, email, password1, password2, mobileNo]);

	const registerUser = async (eventSubmit) => {
		eventSubmit.preventDefault()
		
		const isRegistered =  await fetch('https://frozen-bayou-69638.herokuapp.com/users/register',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
			    firstName: firstName,
			    lastName: lastName,
			    middleName: middleName,
			    email: email,
			    password: password1,
			    mobileNo: mobileNo
			})
		}).then(response => response.json()).then(dataNakaJson => {
			//console.log(dataNakaJson); //user object
			//create a control structure if success or fail
			//check if the email props has data.
			if (dataNakaJson.email) {
				return true; //Success
			} else {
				//failure
				return false;
			}
		})

		//Create a control structure that will evaluate the result of the fetch method.
		if (isRegistered) {
			//prompt message, clear the components, redirect the user to the login if successful registration.
			
			//display a message that will confirm to the user that registration is successful.
			await Swal.fire({
				icon: 'success',
				title: 'Registration Successful',
				text: 'Thank you for creating an Account'
			});
			//reset the registration form
			setFirstName('');
			setMiddleName('');
			setLastName('');
			setEmail('');
			setMobileNo('');
			setPassword1('');
			setPassword2('');

			window.location.href = "/login";
		}	else {
				Swal.fire({
				icon: 'error',
				title: 'Something Went Wrong',
				text: 'Email might be taken! Try another.'
			});
		}
	};

	return (
		user.id 
		? <Navigate to="/" replace={true} />
		:
		<div className="regBg">
			
			<Container className="min-vh-100">
				<Row>
					<Col className="mt-4">
						<div className="regForm">
							{
								isAllowed ?
								<h1 className="text-center text-info pt-5">Signing Up Ready!</h1>
								:
								<h1 className="text-center pt-5">Create Account</h1>
							}
							
							<Form className="register" onSubmit={e => registerUser(e)}>
								{/*First Name Field*/}
								<Form.Group>
									<Form.Label>First Name: </Form.Label>
									<Form.Control type="text" 
									placeholder="Enter your First Name" 
									required
									value={firstName}
									onChange={event=> setFirstName(event.target.value)}
									/>
								</Form.Group>
								{/*Middle Name Field*/}
								<Form.Group>
									<Form.Label>Middle Name: </Form.Label>
									<Form.Control type="text" 
									placeholder="Enter your Middle Name" 
									required
									value={middleName}
									onChange={event=> setMiddleName(event.target.value)}
									/>
								</Form.Group>
								{/*Last Name Field*/}
								<Form.Group>
									<Form.Label>Last Name: </Form.Label>
									<Form.Control 
										type="text"
										placeholder="Enter your Last Name"
										required
										value={lastName}
										onChange={e=>setLastName(e.target.value)}
									/>
								</Form.Group>
								{/*Emil Address Field*/}
								<Form.Group>
									<Form.Label>Email Address: </Form.Label>
									<Form.Control type="email" 
									placeholder="Insert your Email Address" 
									required
									value={email}
									onChange={e=>setEmail(e.target.value)}
									/>
								</Form.Group>
								{/*Mobile Number Field*/}
								{/*Customize this component so that you will get the correct format for the mobile Number*/}
								<Form.Group>
									<Form.Label>Mobile Number: </Form.Label>
									<Form.Control type="number" 
									placeholder="Insert your Mobile No." 
									required
									value={mobileNo} 
									onChange = {e=>setMobileNo(e.target.value)}
									/>
									{
										isMobileValid ?
										<span className="text-success">Mobile No. is Valid!</span>
										:
										<span className="text-muted">Mobile No. Should Be 11-digits</span>
									}
								</Form.Group>
								{/*Password Field*/}
								<Form.Group>
									<Form.Label>Password: </Form.Label>
									<Form.Control type="password" 
									placeholder="Enter your password"
									required
									value={password1} 
									onChange={e => setPassword1(e.target.value)}
									/>
								</Form.Group>
								{/*Confirm Password Field*/}
								<Form.Group>
									<Form.Label>Confirm Password: </Form.Label>
									<Form.Control type="password" 
									placeholder="Confirm your password" 
									required
									value={password2} 
									onChange={e => setPassword2(e.target.value)}
									/>
									{
										isMatched ?<span className="text-success passWord">Passwords Matched!</span>
										:
										<span className="text-danger passWord">Passwords Should Match!</span>
									}
								</Form.Group>

								{/*Register Button*/}
								<div className="d-flex justify-content-center">
								{
								isActive ? <Button type="submit" className="btn-secondary my-3 registerButton">Register </Button>
								:<Button type="submit" className="btn-secondary my-3 registerButton" disabled>Register </Button>
								}
								</div>
							</Form>
						</div>

					</Col>
				</Row>
			</Container>
		</div>
		);
};