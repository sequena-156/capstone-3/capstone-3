import {Container, Form, InputGroup, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useState, useEffect } from 'react';
import {useParams } from 'react-router-dom';


export default function UpdateProduct(prod) {

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	
	let token = localStorage.getItem('accessToken')
	
	const [productInfo, setProductInfo] = useState({
		name: null,
		description: null,
		price: null
	});

	const {id} = useParams();
	console.log({id})
	useEffect(()=>{
		fetch(`https://frozen-bayou-69638.herokuapp.com/products/${id}`).then(res => res.json()).then(convertedData =>{	
			setProductInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price
			})
		});
	},[id])


	const updateProd = async (event) => {
		event.preventDefault()

	const isUpdated = await fetch(`https://frozen-bayou-69638.herokuapp.com/products/${id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		}).then(response => response.json()).then(data =>{
			console.log(data)
			
			if(typeof data !== "undefined"){
				return true;
			} else {
				
				return false;
			}

		})

		if (isUpdated) {
			await Swal.fire({
				title: "Success",
				icon: "success",
				text: `${productInfo.name} successfully updated!`
			})
			setName("")
			setDescription("")
			setPrice(0)
			window.location.href = "/products/all"
		} else {
				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Something went wrong!"
				})
			}
		};
		const archiveToggle = (event) => {

			fetch(`https://frozen-bayou-69638.herokuapp.com/products/${id}/archive`, {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				}
			})
			.then(result => result.json())
			.then(result => {
				console.log(result)
	
				if(result === true){
					Swal.fire({
						title: "Success",
						icon: "success",
						text: `Product successfully Archived!`
					})
					
				} else {

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again"
					})
				}
			})
		}
		const reactivateToggle = (event) => {

			fetch(`https://frozen-bayou-69638.herokuapp.com/products/${id}/reactivate`, {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				}
			})
			.then(result => result.json())
			.then(result => {
				console.log(result)
	
				if(result === true){
					Swal.fire({
						title: "Success",
						icon: "success",
						text: `Product successfully Reactivated!`
					})
					
				} else {

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again"
					})
				}
			})
		}

		return (
		<div className="min-vh-100 updateProd">
			<Container>
				<h1 className="text-center pt-5">Update Product</h1>
				<Form onSubmit={e=>updateProd(e)} >
					
					<Form.Group>
						<Form.Label>Product Name:</Form.Label>
						<Form.Control type="text" 
						placeholder="Enter Product Name" 
						required
						value={name}
						onChange={(e)=> setName(e.target.value)}
						/>
					</Form.Group>
					
					<Form.Group>
						<Form.Label>Product Description:</Form.Label>
						<Form.Control type="text" 
						placeholder="Enter Product Description" 
						required
						value={description}
						onChange={(e)=> setDescription(e.target.value)}
						/>
					</Form.Group>
				
					{/*Price Field*/}
						<Form.Label>Price:</Form.Label>
					<InputGroup>
						<InputGroup.Text>PHP</InputGroup.Text>
						<Form.Control type="number" 
						placeholder="Enter Price" 
						required
						value={price}
						onChange={(e)=> setPrice(e.target.value)}
						/>
					</InputGroup>
					<Button type="submit" 
					className="btn-info mb-3 my-3">Update Product</Button>
				</Form>
				<Form>
				
						<Button variant="outline-warning" 
						onClick={reactivateToggle}
						> Reactivate</Button>
						</Form>
						<Form>

						<Button variant="outline-dark" 
						onClick={archiveToggle}
						>Archived</Button>
				
				</Form>

			</Container>
		</div>
		);
	};

