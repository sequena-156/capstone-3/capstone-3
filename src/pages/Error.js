import Header from '../components/Banner';
import {Container} from 'react-bootstrap';

//Pass down 'Props'
const data = {
	title: '404 Page Not Found',
	content: 'The page you are looking for Does Not Exist'
}

export default function ErrorPage(){
	return(
		<Container className="min-vh-100">
		<Header bannerData={data} />
		</Container>
		);
};