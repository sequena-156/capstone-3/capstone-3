import Hero from '../components/Banner';
import { useState, useEffect, useContext} from 'react';
import AllProductsCard from '../components/AllProductsCard';
import { Container, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';

const data = {
	title: "All Products",
	content: "Available and Unavailable"
}

export default function AllProducts() {
	const { user } = useContext(UserContext)

	const [productsCollection, setProductCollection] = useState([]);

		 let token = localStorage.getItem('accessToken');
		

	useEffect(()=>{
		
	
		

	fetch('https://frozen-bayou-69638.herokuapp.com/products/all', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(res => res.json())
		.then(convertedData => {
			setProductCollection(convertedData.map(product => {
				return(
					<AllProductsCard key={product._id} productsProp={product} />
					)
				}))
			
		},[user, token, productsCollection]);
})
console.log(user.isAdmin)
	return(
		
		 <div className="min-vh-100 allProd">
			<Hero bannerData={data}/>
				<Container> 
					<Row>
						<Col>
						 {productsCollection}
						</Col>
					</Row>
				</Container>
			</div>
		)
} 