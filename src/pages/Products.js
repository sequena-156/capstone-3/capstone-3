import Hero from '../components/Banner';
import { useState, useEffect } from 'react';
import ProductCard from '../components/ProductCard';
import { Container, Row, Col } from 'react-bootstrap';

const data = {
	title: "Products Catalog",
	content: "Best Gears of 2022"
}

export default function Products() {

	const [productsCollection, setProductCollection] = useState([]);
	useEffect(()=>{
		fetch('https://frozen-bayou-69638.herokuapp.com/products/').then(res => res.json()).then(convertedData => {
			setProductCollection(convertedData.map(product => {
				return(
					<ProductCard key={product._id} productProp={product} />
					)
			})) 
		});
	},[]);

	return(
		<>
		<div   className="min-vh-100 productsBg">
			<Hero bannerData={data}/>
				<Container> 
					<Row>
						<Col>
						{productsCollection}
						</Col>
					</Row>
				</Container>
			</div>
		</>
		)
}