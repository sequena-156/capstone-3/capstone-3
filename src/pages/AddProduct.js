import Hero from '../components/Banner';
import { useState, useEffect } from 'react';
import {Container, Form, InputGroup, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';



const data = {
	title: 'Create Product Page'
}

export default function AddProduct() {
	
	
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	const [isActive, setIsActive] = useState(false);
	let token = localStorage.getItem('accessToken')
	useEffect(() =>{
		if(name !== '' && description !== '' && price !== 0){
			setIsActive(true);
		} else{
			setIsActive(false);
		}
	},[name, description, price]);

	const createProduct = async (event) => {
		event.preventDefault()

		const isCreated = await fetch('https://frozen-bayou-69638.herokuapp.com/products/create', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		}).then(response => response.json()).then(data =>{
			console.log(data)
			if(data.name){
				return true;
			} else{
				return false;
			}
		})
		if (isCreated) {
			
				await Swal.fire({
					icon: 'success',
					title: 'Successfully Created Product',
				})
				setName('');
				setDescription('');
				setPrice('');

				window.location.href = '/products';

		} else {
					Swal.fire({
					icon: 'error',
					title: 'Something Went Wrong'
				});
		}
};
	return (
		
		<div className="min-vh-100">
			<Hero bannerData={data} />

			<Container>
				<h4 className="text-center">Input Details Here</h4>
				<Form onSubmit={e=>createProduct(e)} >
				
					<Form.Group>
						<Form.Label>Product Name:</Form.Label>
						<Form.Control type="text" 
						placeholder="Enter Product Name" 
						required
						value={name}
						onChange={e=> setName(e.target.value)}
						/>
					</Form.Group>
				
					<Form.Group>
						<Form.Label>Product Description:</Form.Label>
						<Form.Control type="text" 
						placeholder="Enter Product Description" 
						required
						value={description}
						onChange={e=> setDescription(e.target.value)}
						/>
					</Form.Group>
				
						<Form.Label>Price:</Form.Label>
					<InputGroup>
						<InputGroup.Text>PHP</InputGroup.Text>
						<Form.Control type="number" 
						placeholder="Enter Price" 
						required
						value={price}
						onChange={e=> setPrice(e.target.value)}
						/>
					</InputGroup>
					{
						isActive ? <Button type="submit" className="btn-secondary mb-3 my-3">Create Product</Button>
						:<Button type="submit" className="btn-secondary mb-3 my-3" disabled>Create Product</Button>
					}
				</Form>
			</Container>
		</div>
	
		
		);
	};

